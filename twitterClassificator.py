import time
from gensim import corpora, models
from collections import defaultdict
from statistics import mode
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
from nltk.classify.scikitlearn import SklearnClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.svm import LinearSVC
from nltk.classify import ClassifierI
import pickle
import nltk
import random
import json
import logging
import sys
import os

# TRANSLATE ALL COMMENTS TO ENGLISH..!

# important tweepy stuff
access_token = "930797298715676673-wDaguH4HWTFZ2nqGUvXfYOkw4VAoLbc"
access_token_secret = "HaJ7ZXogMtjKYuh8ed0j0V7BvKPVvV94ljghmUO3b6ziJ"
consumer_key = "luD07hwbEFg3p4mKaDbRu7ZRb"
consumer_secret = "lbDvIGBeQ4mVKw2T4pmEkNOxz8TYDOSzVFHuVTmRc4v68bHiuG"
# delete the logging!!!
logging.basicConfig(format='%(message)s', level=logging.INFO)

stoplist = set('for a of the and to in \n . ,'.split())
dontStartWith = 'https://'

recentTweets = {}

runningStreams = {'lang': [], 'hashtags': []}


# streaming tweets and writing them to file
class StdOutListener(StreamListener):
    def on_data(self, data):
        try:
            data = json.loads(data)
            if not ('retweeted_status' in data) and not data['in_reply_to_status_id'] and \
                    len(data['entities']['hashtags']) > 0 and data['text'][:2].lower() != 'rt':
                for tag in data['entities']['hashtags']:
                    hashtag = tag['text'].lower()
                    language = data['lang'].lower()

                    if hashtag not in recentTweets:
                        recentTweets[hashtag] = [data['text']]
                    else:
                        for index, text in enumerate(recentTweets[hashtag]):
                            if data['text'] == text:
                                recentTweets[hashtag].append(recentTweets.pop(index))
                                return
                        recentTweets[hashtag].append(data['text'])
                        if len(recentTweets[hashtag]) > 10:
                            recentTweets[hashtag].pop(0)

                    if not os.path.isdir('training/{0}/{1}/'.format(language, hashtag)):
                        if not os.path.isdir('training/{0}/'.format(language)):
                            if not os.path.isdir('training/'):
                                os.makedirs('training')
                            os.makedirs('training/{0}'.format(language))
                        os.makedirs('training/{0}/{1}'.format(language, hashtag))

                    if not os.path.isfile("training/{0}/{1}/tweets.txt".format(language, hashtag)):
                        fileForStoringStreams = open("training/{0}/{1}/tweets.txt".format(language, hashtag), "w")
                    else:
                        fileForStoringStreams = open("training/{0}/{1}/tweets.txt".format(language, hashtag), "a+")
                    tweetToWrite = dict()
                    tweetToWrite['id'] = data['id_str']
                    tweetToWrite['text'] = data['text']
                    tweetToWrite['hashtags'] = [hashtag['text'].lower() for hashtag in data['entities']['hashtags']]
                    tweetToWrite['location'] = {'geo': data['geo'],
                                                'coordinates': data['coordinates'],
                                                'place': data['place']}
                    tweetToWrite['user'] = {'id': data['user']['id_str'],
                                            'name': data['user']['name'],
                                            'screen_name': data['user']['screen_name'],
                                            'location': data['user']['location']}
                    fileForStoringStreams.write(json.dumps(tweetToWrite) + '\n')
                    fileForStoringStreams.close()
                # print data['text'] + '\n'
                # exit()
            return True
        except:
            return True

    def on_error(self, status):
        print status


# setting the tweepy and streaming parameters
def twitterStream():

    listener = StdOutListener()
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)

    languages = runningStreams['lang']
    hashtags = runningStreams['hashtags']

    global stream
    stream = Stream(auth, listener)
    stream.filter(track=hashtags, languages=languages, async=True)


# lists tweets read from a file for a un-categorized hashtags and inputs a number for amount of categories to be
# trained for it
def listTweets():
    hashtagLengths = {}
    fileMap = {}
    tweetNumberThreshold = 100

    if os.path.isdir('training/'):
        if sum(os.path.isdir('training/' + lang) for lang in os.listdir('training')) > 0:
            for lang in os.listdir('training'):
                if sum(os.path.isdir('training/{0}/{1}'.format(lang, hashtag))
                       for hashtag in os.listdir('training/' + lang)) > 0:
                    for hashtag in os.listdir('training/' + lang):
                        if os.path.isfile("training/{0}/{1}/tweets.txt".format(lang, hashtag)):
                            tweetFileLen = len([line for line in
                                                open("training/{0}/{1}/tweets.txt".format(lang, hashtag), 'r')])
                            if tweetFileLen > tweetNumberThreshold:
                                hashtagLengths['{0} ({1})'.format(hashtag, lang)] = tweetFileLen
                                if lang not in fileMap:
                                    fileMap[lang] = []
                                fileMap[lang].append(hashtag)
                else:
                    print 'No data, stream first please...\n'
                    return
        else:
            print 'No data, stream first please...\n'
            return
    else:
        print 'No data, stream first please...\n'
        return

    for key in sorted(hashtagLengths, key=hashtagLengths.__getitem__):
        print str(hashtagLengths[key]) + '   ' + key

    while True:
        print '\n\nThese are the gathered data for all hashtags that have above ' \
              '{0} examples...'.format(str(tweetNumberThreshold))

        if len(hashtagLengths) > 0:
            print ('\n\nPlease input a hashtag and language from these sets to analize its tweets '
                   '(eg. \'python en\' for python hashtag on english),\n'
                   'a number to change the example number threshold,\n'
                   'or \'return\' to return to main menu')
        else:
            print ('\n\nNo data matcing this criteria...\n'
                   'Please input a number to change the threshold or \'return\' to go to main manu...')

        inputedValue = raw_input().lower()

        if inputedValue in ('', None):
            continue
        if inputedValue == 'return':
            return
        if '#' in inputedValue:
            inputedValue.replace('#', '')
        if len(inputedValue.split(' ')) == 2:
            if inputedValue.split(' ')[1] in fileMap:
                if inputedValue.split(' ')[0] in fileMap[inputedValue.split(' ')[1]]:
                    break
        try:
            tweetNumberThreshold = int(inputedValue)
            hashtagLengths = {}
            fileMap = {}
            for lang in os.listdir('training'):
                for hashtag in os.listdir('training/' + lang):
                    if os.path.isfile("training/{0}/{1}/tweets.txt".format(lang, hashtag)):
                        tweetFileLen = len([line for line in
                                            open("training/{0}/{1}/tweets.txt".format(lang, hashtag), 'r')])
                        if tweetFileLen > tweetNumberThreshold:
                            hashtagLengths['{0} ({1})'.format(hashtag, lang)] = tweetFileLen
                            if lang not in fileMap:
                                fileMap[lang] = []
                            fileMap[lang].append(hashtag)

            for key in sorted(hashtagLengths, key=hashtagLengths.__getitem__):
                print str(hashtagLengths[key]) + '   ' + key
        except:
            print 'Bad input...'

    print '\nHere are some tweets collected for #' + inputedValue + '.\n' + \
          'They are being listed in a 10 by 10 fashion, press enter for more or type \'break\' to continue...\n'

    inputedLanguage = inputedValue.split(' ')[1]
    inputedHashtag = inputedValue.split(' ')[0]
    tweets = readTweetsFromFile(inputedLanguage, inputedHashtag)

    while True:
        for counter in range(0, len(tweets), 10):
            if len(tweets) - counter >= 10:
                print '{0} - {1} of {2} for #{3}'. \
                    format(counter + 1, counter + 10, len(tweets) + 1, inputedValue)
                for index, tweet in enumerate(tweets[counter:counter + 10]):
                    print ('{0}. '.format(index + counter + 1) + tweet['text'].replace('\n\n', '\n'))
            else:
                print '{0} - {1} of {1} for #{2}'. \
                    format(counter + 1, len(tweets) + 1, inputedValue)
                for index, tweet in enumerate(tweets[counter:]):
                    print ('{0}. '.format(index + counter + 1) + tweet['text'].replace('\n\n', '\n'))
            if raw_input().lower() == 'break':
                break
        print '\nSo those are the tweets in #{}.'.format(inputedValue)

        shouldBreak = False
        choice = ''
        while not shouldBreak:
            print 'Enter a number now if you know into how many categories you might group them, ' \
                  'or type \'repeat\' to repeat them or \'return\' to return to main menu...'
            choice = raw_input()
            if choice.lower() == 'repeat':
                break
            elif choice.lower() == 'return':
                print ''
                theTalker()
            else:
                try:
                    choice = int(choice)
                    if choice > 1:
                        shouldBreak = True
                    else:
                        print('Please enter a number greater than 1...')
                except:
                    pass
            if not shouldBreak:
                print 'Bad input...\n'
        print ''
        if shouldBreak:
            if not os.path.isdir('training/{0}/{1}/'.format(inputedLanguage, inputedHashtag)):
                if not os.path.isdir('training/{0}/'.format(inputedLanguage)):
                    if not os.path.isdir('training/'):
                        os.makedirs('training')
                    os.makedirs('training/{0}'.format(inputedLanguage))
                os.makedirs('training/{0}/{1}'.format(inputedLanguage, inputedHashtag))
            findNewCategories(choice, tweets, inputedLanguage, inputedHashtag)
            return


# reads tweets from a tweets.txt file and returns it as an array of dicts representing tweets
def readTweetsFromFile(languageFilter, hashtagFilter):
    try:
        fileWithTweets = open('training/{0}/{1}/tweets.txt'.format(languageFilter, hashtagFilter), 'r')
        forReturn = [json.loads(tweet) for tweet in fileWithTweets]
        fileWithTweets.close()
        return forReturn
    except:
        print sys.exc_info()[0]
        print ('The file you have selected is not valid or is corrupt...\n'
               'Returning to main menu...\n\n')
        theTalker()


# creates a LSI model of tweets and its dictionary
def findNewCategories(topicAmount, tweets, language, hashtag):
    global stoplist, dontStartWith

    # takes the tweet corpus and converts it into the training set texts leaving out stoplist words and links
    texts = [stripAndSplitTweet(tweet) for tweet in tweets]

    # removes words from our training set that only appear once in the whole corpus
    frequency = defaultdict(int)
    for text in texts:
        for token in text:
            frequency[token] += 1
    texts = [[token for token in text if frequency[token] > 1]
             for text in texts]

    # created word corpus (with number of appearances) and written to disk
    dictionary = corpora.Dictionary(texts)

    dictionary.save('training/{0}/{1}/corporaDict.dict'.format(language, hashtag))

    # mapped corpus from 'texts' based on the tokens saved in 'dictionary'
    corpus = [dictionary.doc2bow(text) for text in texts]
    corpora.MmCorpus.serialize('training/{0}/{1}/mmCorpus.mm'.format(language, hashtag), corpus)

    # transformes the dictionary into vectors
    tfidf = models.TfidfModel(corpus)
    corpus_tfidf = tfidf[corpus]
    
    # created an LSI model with TFIDF mapped corpus
    lsi = models.LsiModel(corpus_tfidf, id2word=dictionary, num_topics=topicAmount)
    lsi.save('training/{0}/{1}/lsiModel.lsi'.format(language, hashtag))
    nextStep = raw_input('\'menu\' for main menu or nothing to continue to topic naming...\n')
    if nextStep.lower() == 'menu':
        theTalker()
        return
    evaluateLsiTopics(language, hashtag, tweets)


# fits tweets into the LSI models topics, lets the user analyse and name the topics,
# and adds LSI topic prediction into the corpus for SL training with labeled text
def evaluateLsiTopics(language, hashtag, tweets):
    # loads the model and word dictionary for evaluation
    lsi = models.LsiModel.load('training/{0}/{1}/lsiModel.lsi'.format(language, hashtag))
    dictionary = corpora.Dictionary.load('training/{0}/{1}/corporaDict.dict'.format(language, hashtag))
    corpus = corpora.MmCorpus('training/{0}/{1}/mmCorpus.mm'.format(language, hashtag))

    categorisedTweets = dict()

    # goes trough the corpus and fits the tweets into topic
    # (test for fitting into top X groups or discarding if it overally fits in most groups)
    for tweet in tweets:
        strippedTweet = tweet['text']
        while '#' in strippedTweet or '@' in strippedTweet or '\n' in strippedTweet:
            strippedTweet = strippedTweet.replace('#', '').replace('@', '').replace('\n', ' ')
        vec_bow = dictionary.doc2bow(strippedTweet.lower().split())
        vec_lsi = lsi[vec_bow]
        vec_lsi = sorted(vec_lsi, key=lambda item: item[1], reverse=True)
        if len(vec_lsi) == 0:
            topicWithTweets = dict()
            if 'Uncategorised' not in categorisedTweets:
                categorisedTweets['Uncategorised'] = list()
            topicWithTweets['text'] = tweet['text']
            topicWithTweets['prob'] = 0
            topicWithTweets['id'] = tweet['id']
            categorisedTweets['Uncategorised'].append(topicWithTweets)
            continue
        maxTopicsPerTweet = 3
        counter = 0
        for topic in vec_lsi:
            if topic[1] < 1:
                break
            counter += 1
            topicWithTweets = dict()
            if not topic[0] in categorisedTweets:
                categorisedTweets[topic[0]] = list()
            topicWithTweets['text'] = tweet['text']
            topicWithTweets['prob'] = topic[1]
            topicWithTweets['id'] = tweet['id']
            categorisedTweets[topic[0]].append(topicWithTweets)
            if not counter < maxTopicsPerTweet:
                break
        if counter == 0:
            topicWithTweets = dict()
            topicWithTweets['text'] = tweet['text']
            topicWithTweets['prob'] = vec_lsi[0][1]
            topicWithTweets['id'] = tweet['id']
            if not vec_lsi[0][0] in categorisedTweets:
                categorisedTweets[vec_lsi[0][0]] = list()
            categorisedTweets[vec_lsi[0][0]].append(topicWithTweets)

    # lists the topic per topic with tweets fitted into it and maps the new topic name if given
    if not os.path.isfile("training/{0}/{1}/topicNameMap.txt".format(language, hashtag)):
        topicNameMap = dict()
    elif not os.stat("training/{0}/{1}/topicNameMap.txt".format(language, hashtag)).st_size == 0:
        topicNameMap = json.load(open("training/{0}/{1}/topicNameMap.txt".format(language, hashtag)))
    else:
        topicNameMap = dict()
    for key in sorted(categorisedTweets):
        if str(key) not in topicNameMap:
            topicNameMap[str(key)] = str(key)
        print '\nTOPIC {0}'.format(topicNameMap[str(key)])
        for tweet in sorted(categorisedTweets[key], key=lambda tvit: tvit['prob'], reverse=True)[:20]:
            print str(tweet['prob']) + ' ' + tweet['text']
        newName = raw_input('Input a new name for this topic: (nothing to skip and break to go to main menu)\n')
        if newName.lower() == 'break':
            break
        elif newName is not None and newName != '':
            topicNameMap[str(key)] = newName
        topicNameMapFile = open("training/{0}/{1}/topicNameMap.txt".format(language, hashtag), "w")
        topicNameMapFile.write(json.dumps(topicNameMap))
        topicNameMapFile.close()
    print 'You have finished naming the categories, adding them to the training set and returning to main menu now...\n'

    # adding the tweets with named categories to the training corpus
    for key in categorisedTweets:
        topicNameMap = json.load(open("training/{0}/{1}/topicNameMap.txt".format(language, hashtag)))
        if str(key) not in topicNameMap:
            continue
        try:
            isItANumber = int(topicNameMap[str(key)])
            if type(isItANumber) is int:
                continue
        except:
            for tweet in categorisedTweets[key]:
                addToLabeledCorpus(language, hashtag, tweet['text'], topicNameMap[str(key)], tweet['id'],
                                   fromUser=False)
    theTalker()


# adds a labeled tweet into its corpus file for later SL training
def addToLabeledCorpus(language, hashtag, tweet, topic, tweetId, fromUser=True):
    if not os.path.isfile("training/{0}/{1}/LabeledCorpus.txt".format(language, hashtag)):
        corpusFile = open("training/{0}/{1}/LabeledCorpus.txt".format(language, hashtag), "w")
    else:
        corpusFile = open("training/{0}/{1}/LabeledCorpus.txt".format(language, hashtag), "a+")
    dictToWrite = dict()
    dictToWrite[tweetId] = {}
    dictToWrite[tweetId]['topic'] = topic
    dictToWrite[tweetId]['tweet'] = tweet
    dictToWrite[tweetId]['fromUser'] = fromUser
    corpusFile.write(json.dumps(dictToWrite) + '\n')
    corpusFile.close()


# loads labeled corpus of tweets and eliminates the automatically made label where the is a user made label for a tweet
def loadLabeledCorpus(language, hashtag):
    try:
        corpusFile = open("training/{0}/{1}/LabeledCorpus.txt".format(language, hashtag), "r")
        tweetCorpus = {}
        for line in corpusFile:
            tweet = json.loads(line)
            for tweetId in tweet:
                if tweetId not in tweetCorpus:
                    tweetCorpus[tweetId] = []
                tweetCorpus[tweetId].append(tweet[tweetId])

        for tweetId in tweetCorpus:
            if len(tweetCorpus[tweetId]) > 1:
                for index, tweet in enumerate(tweetCorpus[tweetId]):
                    if not bool(tweet['fromUser']):
                        tweetCorpus[tweetId].pop(index)
                        break

        corpusSets = []

        for tweetId in tweetCorpus:
            for tweet in tweetCorpus[tweetId]:
                corpusSets.append((stripAndSplitTweet(tweet['tweet']), tweet['topic']))

        return corpusSets
    except:
        print ('File error for \'training/{0}/{1}/LabeledCorpus.txt\', '
               'returning to main menu'.format(language, hashtag))
        theTalker()


# a simple function that returns a list of words without stopwords and other stuff not suitable for training
def stripAndSplitTweet(tweet):
    return list(w.replace('#', '').replace('@', '').replace('.', '').replace(',', '')
                for w in tweet.lower().split()
                if (w not in stoplist) and (not w.startswith(dontStartWith)))


# a training model that trains 5 different classifiers and saves them for later use
def trainNNModel(language, hashtag):
    tweetCorpus = loadLabeledCorpus(language, hashtag)
    random.shuffle(tweetCorpus)

    allWords = [word for tweet in tweetCorpus for word in tweet[0]]

    allWords = nltk.FreqDist(allWords)

    global wordFeatures
    wordFeatures = list(allWords.keys())[:3000]

    featureSets = [(findFeatures(tweet[0]), tweet[1]) for tweet in tweetCorpus]

    random.shuffle(featureSets)

    trainSet = featureSets[:int((len(featureSets) * 9) // 10)]
    testSet = featureSets[int((len(featureSets) * 9) // 10):]

    NB_classifier = nltk.NaiveBayesClassifier.train(trainSet)

    MNB_classifier = SklearnClassifier(MultinomialNB()).train(trainSet)

    LRC_classifier = SklearnClassifier(LogisticRegression()).train(trainSet)

    SGDC_classifier = SklearnClassifier(SGDClassifier()).train(trainSet)

    LSVC_classifier = SklearnClassifier(LinearSVC()).train(trainSet)

    classifierList = [NB_classifier, MNB_classifier, LRC_classifier, SGDC_classifier, LSVC_classifier]

    votedClassifier = VoteClassifier(classifierList)
    print 'Accuracy of the trained model is {0}%! '.format(
        str(((nltk.classify.accuracy(votedClassifier, testSet)) * 100)))

    dictToSave = {'classifier': votedClassifier, 'features': wordFeatures}

    classifierFile = open("training/{0}/{1}/classifier.picle".format(language, hashtag), 'wb')
    pickle.dump(dictToSave, classifierFile)
    classifierFile.close()

    theTalker()


# a classifier that acts like the average of 5 different classifiers for higher accuracy
class VoteClassifier(ClassifierI):
    def labels(self):
        pass

    def __init__(self, *classifiers):
        self._classifiers = classifiers[0]

    def classify(self, features):
        votes = []
        for c in self._classifiers:
            v = c.classify(features)
            votes.append(v)
        return getMode(votes)

    def conficence(self, features):
        votes = []
        for c in self._classifiers:
            v = c.classify(features)
            votes.append(v)

        choice_votes = votes.count(getMode(votes))
        conf = float(choice_votes) / len(votes)
        return conf


# guess to be deleted
def getMode(aList):
    try:
        return mode(aList)
    except:
        max(set(aList), key=aList.count)


# converts given tweet into a list of its features witch is later used by the naive bayes training
def findFeatures(tweet, wFeatures=None):
    if wFeatures is None:
        words = set(tweet)
        features = {}
        for w in wordFeatures:
            features[w] = (w in words)
        return features
    else:
        words = set(tweet)
        features = {}
        for w in wFeatures:
            features[w] = (w in words)
        return features


# streams tweets 5 by 5 where the user can re-label them and adds them to the labeled corpus for further re-training
def streamCategorisedTweets(language, hashtag):
    classifierFile = open("training/{0}/{1}/classifier.picle".format(language, hashtag), 'rb')
    dictToLoad = pickle.load(classifierFile)
    classifierFile.close()

    if language not in runningStreams['lang']:
        runningStreams['lang'].append(language)
    if hashtag not in runningStreams['hashtags']:
        runningStreams['hashtags'].append(hashtag)
    twitterStream()

    classifier = dictToLoad['classifier']

    wFeatures = dictToLoad['features']

    tweetFile = open("training/{0}/{1}/tweets.txt".format(language, hashtag), "r")

    fileLen = len(tweetFile.readlines())

    tweetFile.close()

    while True:
        tweetFile = open("training/{0}/{1}/tweets.txt".format(language, hashtag), "r")
        newLen = len(tweetFile.readlines())
        tweetFile.close()
        if newLen < fileLen + 5:
            print 'Please wait while we gather more tweets...'
            time.sleep(5)
            continue

        fileLen = newLen

        labelingTweets = readTweetsFromFile(language, hashtag)

        labelingTweets = labelingTweets[-5:]

        # labelingTweets = [json.loads(line) for line in fileLines]

        for index, tweet in enumerate(labelingTweets):
            features = findFeatures(stripAndSplitTweet(tweet['text']), wFeatures=wFeatures)
            topic = classifier.classify(features)
            confidence = classifier.conficence(features)

            text = tweet['text']

            print ('{0}. {1}\nTopic found: {2} with {3}% confidence\n'.format(index + 1,
                                                                              text.strip('\n').encode('utf-8'),
                                                                              topic,
                                                                              confidence*100))

        print ('Please input the index numbers of tweets you would like to re-label it or confirm their label '
               '(e.g. \'1 3 4\').\n'
               'Press enter to continue to a set of 5 newly streamed tweets or type \'return\' to return to main menu')
        inputedChoice = raw_input()

        if inputedChoice == '':
            continue
        elif inputedChoice.lower() == 'return':
            theTalker()
            break
        else:
            try:
                if ' ' in inputedChoice:
                    indexes = [int(index) for index in inputedChoice.split()]
                else:
                    indexes = list([int(inputedChoice)])
                for index in indexes:
                    features = findFeatures(stripAndSplitTweet(labelingTweets[index - 1]['text']),
                                            wFeatures=wFeatures)
                    topic = classifier.classify(features)
                    print ('{0}. {1}\nTopic found: {2}'.format(index,
                                                               labelingTweets[index - 1]
                                                               ['text'].strip('\n').encode('utf-8'),
                                                               topic))
                    label = raw_input('Please enter a new topic for this tweet or just press enter to confirm it\n')
                    if label == '':
                        label = topic
                    addToLabeledCorpus(language,
                                       hashtag,
                                       labelingTweets[index - 1]['text'],
                                       label,
                                       labelingTweets[index - 1]['id'],
                                       fromUser=True)
            except:
                'Bad input...'


# main controller for interaction with user
def theTalker(isStart=False):
    if isStart:
        print 'Hello world!'
        print 'If this is your first run you might want to just head to the streaming part...'

        hashtagsLearned = []

        if os.path.isdir('training/'):
            if sum(os.path.isdir('training/' + lang) for lang in os.listdir('training')) > 0:
                for lang in os.listdir('training'):
                    if sum(os.path.isdir('training/{0}/{1}'.format(lang, hashtag))
                           for hashtag in os.listdir('training/' + lang)) > 0:
                        for hashtag in os.listdir('training/' + lang):
                            if os.path.isfile("training/{0}/{1}/classifier.picle".format(lang, hashtag)):
                                hashtagsLearned.append({'lang': lang, 'hashtag': hashtag})
        if len(hashtagsLearned) > 0:
            print 'So far you have learned to categorize topics in ' + str(len(hashtagsLearned)) + ' hashtags!'
            print 'Here they are: '
            for hashtag in hashtagsLearned:
                print '{0} ({1})'.format(hashtag['hashtag'], hashtag['lang'])

    while True:
        inputedText = raw_input('Okay, so here are your options now: \n'
                                'a) stream new tweets without categories for initial training\n'
                                'b) analyze streamed tweets and fit them in any number of new categories \n'
                                'c) analyze the categories with tweets that fit them and maybe re-name them? \n'
                                'd) stream new tweets with their categories and label them by wish\n'
                                'e) re-train a certain classifier by hashtag '
                                '(if you have newly aquired tweets or got some tweets labeled) \n'
                                'p.s. this is an automatic loop message, input a/b/c/d/e/f to do something,'
                                ' type \'break\' or \'exit\' to exit and input nothing to do nothing... \n')

        if inputedText.lower() == 'break' or inputedText.lower() == 'exit':
            print 'Goodbye world!'
            sys.exit()

        elif inputedText.lower() == 'a':
            language = raw_input('Please input the language(s) you would like to stream in (e.g. \'en fr es\')')
            if language == '':
                print ('Bad input...')
                continue
            if ' ' in language:
                language = [lang for lang in language.split()]
            else:
                language = [language]
            hashtagsToStream = []
            hashtags = raw_input('Please input the hashtag(s) you would like to stream from '
                                 '(e.g. \'#python #twitter #stream\')')
            if hashtags == '':
                print ('Bad input...')
                continue
            if ' ' in hashtags:
                for hashtag in hashtags.split():
                    if not hashtag.startswith('#'):
                        hashtag = '#' + hashtag
                    hashtagsToStream.append(hashtag)
            else:
                if not hashtags.startswith('#'):
                    hashtags = '#' + hashtags
                hashtagsToStream.append(hashtags)
            runningStreams['lang'] = language
            runningStreams['hashtags'] = hashtagsToStream
            # runningStreams['lang'] = ['en']
            # runningStreams['hashtags'] = ['#python', '#bitcoin', '#tesla', '#ethernum']
            twitterStream()

            theTalker()
            break

        elif inputedText.lower() == 'b':
            listTweets()

        elif inputedText.lower() == 'c':
            hashtag = raw_input('Please input the hashtag you are sure is already trained (its still beta version!)\n')
            evaluateLsiTopics('en',
                              hashtag,
                              readTweetsFromFile(hashtag, 'en'))

        elif inputedText.lower() == 'd':
            hashtagsLearned = []
            if os.path.isdir('training/'):
                if sum(os.path.isdir('training/' + lang) for lang in os.listdir('training')) > 0:
                    for lang in os.listdir('training'):
                        if sum(os.path.isdir('training/{0}/{1}'.format(lang, hashtag))
                               for hashtag in os.listdir('training/' + lang)) > 0:
                            for hashtag in os.listdir('training/' + lang):
                                if os.path.isfile("training/{0}/{1}/classifier.picle".format(lang, hashtag)):
                                    hashtagsLearned.append({'lang': lang, 'hashtag': hashtag})

            if len(hashtagsLearned) > 0:
                print 'These are the hashtags you can stream and classify:'
                while True:
                    for index, hashtag in enumerate(hashtagsLearned):
                        print '{2}. {0} ({1})'.format(hashtag['hashtag'],
                                                      hashtag['lang'],
                                                      index + 1)
                    choice = raw_input('Please enter the number of the hashtag you would like to live stream from '
                                       'or \'return\' to go back to main menu\n')
                    if choice.lower() == 'return':
                        theTalker()
                        break
                    try:
                        choice = int(choice.replace('.', '')) - 1
                    except:
                        print 'Bad input, please try again....'
                        continue
                    choice = choice
                    streamCategorisedTweets(hashtagsLearned[choice]['lang'], hashtagsLearned[choice]['hashtag'])
                    break

        elif inputedText.lower() == 'e':
            hashtagsLearned = []

            if os.path.isdir('training/'):
                if sum(os.path.isdir('training/' + lang) for lang in os.listdir('training')) > 0:
                    for lang in os.listdir('training'):
                        if sum(os.path.isdir('training/{0}/{1}'.format(lang, hashtag))
                               for hashtag in os.listdir('training/' + lang)) > 0:
                            for hashtag in os.listdir('training/' + lang):
                                if os.path.isfile("training/{0}/{1}/classifier.picle".format(lang, hashtag)):
                                    hashtagsLearned.append({'lang': lang, 'hashtag': hashtag})

            if len(hashtagsLearned) > 0:
                print 'These are the hashtags with trained classifiers:'
                while True:
                    for index, hashtag in enumerate(hashtagsLearned):
                        print '{2}. {0} ({1})'.format(hashtag['hashtag'], hashtag['lang'], index + 1)
                    choice = raw_input('Please enter the number of the hashtag you would like to re train '
                                       'or \'return\' to go back to main menu\n')
                    if choice.lower() == 'return':
                        theTalker()
                        break
                    try:
                        choice = int(choice.replace('.', '')) - 1
                    except:
                        print 'Bad input, please try again....'
                        continue
                    choice = choice
                    trainNNModel(hashtagsLearned[choice]['lang'], hashtagsLearned[choice]['hashtag'])
                    break
            else:
                print 'We couldnt find any trained classifiers.... Please train and try again...'


if __name__ == '__main__':
    theTalker(isStart=True)
